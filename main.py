from typing import Optional

from fastapi import FastAPI

app = FastAPI()


@app.get("/items/{item_id}")
def read_item(item_id: int, a: int, b: int):
    return {"item_id": item_id, a + b}

@app.get("/items/{item_id}")
def read_item(item_id: int, a: int, b: int):
    return {"item_id": item_id, a - b}

@app.get("/items/{item_id}")
def read_item(item_id: int, a: int, b: int):
    return {"item_id": item_id, a * b}

@app.get("/items/{item_id}")
def read_item(item_id: int, a: int, b: int):
    return {"item_id": item_id, a / b}
